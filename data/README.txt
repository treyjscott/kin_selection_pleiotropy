This README describes the contents of the data directory used in "Cooperation loci are more pleiotropic than private loci in Pseudomonas aerugniosa"

# QS Genes
To construct the quorum sensing (QS) cooperation and private gene dataset, I used Table 1 (Table1QS.pdf was converted to excel - Table1QS.xlsx - then to csv - Table1QS.csv) from Schuster et al. 2003. I categorized a subset of these genes as cooperative from Supplemental Table 1 (Table1.pdf was converted to excel- Table1.xlsx - then to csv - Table1.csv) in Belcher et al. 2022. 

# Additional genes
Additional gene sets were taken from Supplemental Tables 2-6 (Table2-6.pdf converted to xlsx) in Belcher et al. (2022). From these supplemental tables, I created individual csvs for each group (Pyochelin.csv, Pyoverdine.csv, EPR.csv, CooperativeAMR.csv, TIVP.csv, T6SS.csv, R_and_F.csv, Porins.csv).

# STRING data
208964.protein.links.full.v11.5.txt - These data were downloaded from the STRING database (https://string-db.org/). 

# GO data
pseudocap.gaf - These data were downloaded from the Pseudomonas genome database (https://www.pseudomonas.com/)

# Orthologs
To convert between PAO annotations and PA14 annotations, I used orthologs downloaded from the Pseudomonoas genome database (https://www.pseudomonas.com/). 

# Gene Expression
Normalized and unnormalized gene expression data are found in PA14_mean_expression.csv and PA14_mean_expression_normalized.csv, respectively. These data were generated with the gene expression data in GSE55197_RAW.