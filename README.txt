This README.txt file was generated on 2022-07-09 by Trey J Scott

### Data is located in the final_data directory!!! ###

GENERAL INFORMATION

1. Title of Dataset: Cooperation genes are more pleiotropic than private genes in the bacterium Pseudomonas aeruginosa

2. Author Information
	A.	Name: Trey J. Scott
		Institution: Washington University in St. Louis
		Email: tjscott@wustl.edu


3. Date of data collection: March 2022 

4. Geographic location of data collection: Washington University in St. Louis, St. Louis, MO, US 

5. Information about funding sources that supported the collection of the data: 
NSF DEB-1753743 and IOS-1656756


SHARING/ACCESS INFORMATION

1. Licenses/restrictions placed on the data: None

2. Links to publications that cite or use the data: https://doi.org/10.1101/2022.06.25.495533

3. Links to other publicly accessible locations of the data: www.gitlab.com/treyjscott/kin_selection_pleiotropy

4. Links/relationships to ancillary data sets: None

5. Was data derived from another source? no

6. Recommended citation for this dataset: https://doi.org/10.1101/2022.06.25.495533


DATA & FILE OVERVIEW

1. File List: 
-analysis.R
-DatasetS1.csv
-DatasetS2.csv
-DatasetS3.csv

2. Relationship between files, if important: analysis.R loads other data files and performs analysis.

3. Additional related data collected that was not included in the current data package: 

4. Are there multiple versions of the dataset? yes/no
	A. If yes, name of file(s) that was updated: 
		i. Why was the file updated? 
		ii. When was the file updated? 


METHODOLOGICAL INFORMATION

1. Description of methods used for collection/generation of data: 
I generated this data from published and online sources. 

2. Methods for processing the data: 
 

3. Instrument- or software-specific information needed to interpret the data: 



DATA-SPECIFIC INFORMATION FOR: DatasetS1.csv

1. Number of variables: 2

2. Number of cases/rows: 316

3. Variable List: 
-Gene, gene ID
-Type, character for whether gene is cooperative or private


DATA-SPECIFIC INFORMATION FOR: DatasetS2.csv

1. Number of variables: 5

2. Number of cases/rows: 5566

3. Variable List: 
-Gene, gene ID
-GOs, GO term pleiotropy
-Protein Interactions, STRING protein interaction pleiotropy
-Score, average protein interaction score from STRING
-Expression Pleiotropy, expression pleiotropy calculated from gene expression data


DATA-SPECIFIC INFORMATION FOR: DatasetS3.csv

1. Number of variables: 3

2. Number of cases/rows: 90

3. Variable List: 
-Gene, gene ID
-Sociality, factor for whether gene is cooperative or private
-Set, factor for the gene set


